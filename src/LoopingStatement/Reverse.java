package LoopingStatement;

import java.util.Scanner;

public class Reverse {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Start point");
        int start=sc.nextInt();
        System.out.println("Enter End point");
        int end=sc.nextInt();

        for(int a=end;a>=start;a--){
            System.out.println(a);
        }
    }
}
