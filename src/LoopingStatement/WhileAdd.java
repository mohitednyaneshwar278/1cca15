package LoopingStatement;
import java.util.Scanner;
public class WhileAdd {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        boolean status=true;
        while (status) {
            System.out.println("1:Addition");
            System.out.println("2:Subtraction");
            System.out.println("3:Exit");
            int choice = sc.nextInt();
            System.out.println("Enter first no");
            int no1 = sc.nextInt();
            System.out.println("Enter Second no");
            int no2 = sc.nextInt();

            if (choice==1) {
                System.out.println(no1+no2);
            } else if (choice==2) {
                System.out.println(no1-no2);
            } else {
                status=false;
            }
        }
    }
}
