package LoopingStatement;
import java.util.Scanner;
public class Summation {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter start point");
        int start=sc.nextInt();
        System.out.println("Enter end point");
        int end=sc.nextInt();
        double sum=0.0;
        for(int a=start;a<=end;a++){
            if(a%2==0){
             System.out.println(a);
             sum+=a;
            }
        }

        System.out.println("Sum:"+sum);

    }
}
