package LoopingStatement;
import java.util.Scanner;
public class WhileSum {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int no;
        int sum=0;
        while(sum<=50) {
            System.out.println("Enter No");
            no=sc.nextInt();
            sum+=no;
        }
        System.out.println("Sum:"+sum);
    }
}
