package LoopingStatement;
import java.util.Scanner;
public class Switch {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Selected Language");
        System.out.println("1:JAVA\n2:PYTHON\n3:PHP");
        int choice=sc.nextInt();

        switch(choice){
            case 1:
                System.out.println("Selected JAVA Language");
            break;
            case 2:
                System.out.println("Selected PYTHON Language");
            break;
            case 3:
                System.out.println("Selected PHP Language");
            break;
            default:
                System.out.println("Invalid Choice");
        }
    }
}
