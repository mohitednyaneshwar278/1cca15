package ARRAYS;

import java.util.Scanner;

public class ArrayDemo3 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        double sum=0.0;
        System.out.println("Enter no of courses");
        int size=sc.nextInt();
        System.out.println("Enter Marks");
        double[] marks=new double[size];

        for(int b=0;b<size;b++){
         marks[b]=sc.nextDouble();
            sum+=marks[b];
        }
        System.out.println("sum:"+sum);
        System.out.println("Percent:"+sum/size);
    }
}
