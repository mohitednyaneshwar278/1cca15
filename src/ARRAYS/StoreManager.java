package ARRAYS;

public class StoreManager {
    static String[] products={"TV","Project","Mobile"};
    static double[] cost={15000,25000,30000};
    static int[] stock={25,10,50};

    void calculateBill(int choice,int qty)
    {
        boolean found=false;
        for (int a=0;a< products.length;a++){
            if(choice==a && qty<=stock[a]) {
                double total = qty * cost[a];
                stock[a]-=qty;
                System.out.println("Total Bill Amount:" + total);
                found=true;
            }
        }
        if(!found){
            System.out.println("Product not found r else out of stock");
        }
    }
}
