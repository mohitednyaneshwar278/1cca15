package ARRAYS;

import java.util.Scanner;

public class Array2Demo3 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Total no of Floor");
        int floors=sc.nextInt();
        System.out.println("Enter Total no of Flats on each Floor");
        int flats=sc.nextInt();

        int[][] data=new int[floors][flats];
        System.out.println("Enter"+(floors*flats)+"flat No");
        //Accept flat no
        for(int a=0;a<floors;a++){
            for(int b=0;b<flats;b++){
                data[a][b]=sc.nextInt();
            }
        }
        System.out.println("===============");
        for(int a=0;a<floors;a++){
            System.out.println("Floor No:"+(a+1));
            System.out.println("_____________________" );
            for(int b=0;b<flats;b++){
                System.out.print("Flat No:"+data[a][b]+"\t");
            }
            System.out.println();
            System.out.println("====================");
        }
    }
}
