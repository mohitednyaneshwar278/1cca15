package ARRAYS;

public class Array3D {
    public static void main(String[] args) {
        int[][][] data=new int[2][2][2];
        //1st row
        data[0][0][0]=10;
        data[0][0][1]=20;
        data[0][1][0]=30;
        data[0][0][1]=40;

        //2nd row
        data[1][0][0]=50;
        data[1][0][1]=60;
        data[1][1][0]=70;
        data[1][1][1]=80;

        //rows
        for(int r=0;r< data.length;r++){
            //cols
            for(int c=0;c<data.length;c++){
                //panels
                for(int p=0;p<data.length;p++){
                    System.out.print(data[r][c][p]+"\t");
                }
            }
            System.out.println();
        }
    }
}
