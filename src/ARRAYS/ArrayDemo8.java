package ARRAYS;

import java.util.Scanner;

public class ArrayDemo8 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        boolean status=true;
        while(status){
            System.out.println("Select Product");
            System.out.println("0:TV\n1:Projector\n2:Mobile\n3:Exit");
            int choice=sc.nextInt();
            System.out.println("Enter Qty");
            int qty=sc.nextInt();

            StoreManager s1=new StoreManager();
            if(choice==0 || choice==1 || choice==2){
                s1.calculateBill(choice,qty);
            }
            else{
                System.out.println("Invalid choice");
                status=false;
            }
        }
    }
}
