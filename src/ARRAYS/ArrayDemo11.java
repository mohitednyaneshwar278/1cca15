package ARRAYS;

import java.util.Scanner;

public class ArrayDemo11 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        billCalculator b1=new billCalculator();
        System.out.println("Enter no of bills");
        int count=sc.nextInt();
        System.out.println("Enter Bill Amounts");
        double[] amounts=new double[count];

        for(int a=0;a< amounts.length;a++){
            amounts[a]=sc.nextInt();
        }
        b1.calculateBill(amounts);
    }
}
