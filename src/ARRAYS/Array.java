package ARRAYS;

public class Array {
    public static void main(String[] args) {
        //Declaration
        int[] data;
        //Size allocation
        data=new int[5];
        //Initialization
        data[0]=100;
        data[1]=200;
        data[2]=300;
        data[3]=400;
        data[4]=500;

        for(int a=0;a<=4;a++){
            System.out.println(data[a]);
        }
    }
}
