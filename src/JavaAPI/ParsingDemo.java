package JavaAPI;

public class ParsingDemo {
    public static void main(String[] args) {
        String s1="35";
        String s2="45";
        System.out.println(s1+s2);

        //Parsing
        int no1=Integer.parseInt(s1);
        int no2=Integer.parseInt(s2);
        System.out.println(no1+no2);

        double d1=35.25;
        double d2=45.25;
        System.out.println(d1+d2);
        String str1=Double.toString(d1);
        String str2=Double.toString(d2);
        System.out.println(str1+str2);
    }
}
