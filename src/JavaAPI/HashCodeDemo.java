package JavaAPI;

public class HashCodeDemo {
    public static void main(String[] args) {
        String s1=new String("JAVA");
        String s2=new String("JAVa");
        String s3=new String("SQL");

        System.out.println(s1==s2);//false
        System.out.println(s1==s3);//false
        System.out.println(s1.hashCode()==s2.hashCode());//true
        System.out.println(s1.hashCode()==s3.hashCode());//false
        System.out.println(s1.hashCode());//2269730
        System.out.println(s2.hashCode());//2269762
        System.out.println(s3.hashCode());//82350

        System.out.println(s1.getClass());
    }
}
