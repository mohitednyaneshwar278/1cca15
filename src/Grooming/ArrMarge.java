package Grooming;

public class ArrMarge {
    public static void main(String[] args) {
        int[] arr1={10,20,30,40,50};
        int[] arr2={60,70,80,90,100};


        int[] arr3=new int[arr1.length+ arr2.length];
        int count=0;

        for(int a:arr1) {
            arr3[count]=a;
            count++;
        }
        for(int a:arr2)
        {
            arr3[count]=a;
            count++;
        }
        for(int a:arr3)
            System.out.print(a+" ");
    }
}
