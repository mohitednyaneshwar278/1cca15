package Grooming;

public class ArrSecMaxNo {
    public static void main(String[] args) {
        int[] arr={10,20,30,40,50,60,60};
        int max1=arr[0];
        int max2=arr[1];
        for(int a:arr)
            if(a>max1){
                max2=max1;
                max1=a;
            }else if(a>max2 && a!=max1){
                max2=a;
            }
        System.out.println("Max1:  "+max1);
        System.out.println("Max2:  "+max2);
    }
}
