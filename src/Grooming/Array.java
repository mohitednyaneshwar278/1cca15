package Grooming;

public class Array {
    public static void main(String[] args) {
        int[] arr1={1,3,5,7};
        int[] arr2={2,4,6,8};

        int n1=arr1.length;
        int n2=arr2.length;

        int[] arr3=new int[n1+n2];
        int ch1=0,ch2=0;

        for(int i=0;i<n1+n2;i++) {

            if (i % 2 == 0)
                arr3[i] = arr1[ch1++];
            else
                arr3[i] = arr2[ch2++];

            System.out.print(arr3[i]);
        }
    }
}
