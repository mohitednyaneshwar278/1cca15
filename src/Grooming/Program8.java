package Grooming;

public class Program8 {
    public static void main(String[] args) {
        int line=5;
        int star=5;
        int ch=1;
        for(int i=0;i<line;i++){
            for(int j=0;j<star;j++){
                if(i==j){
                    System.out.print(ch);
                }else{
                    System.out.print("O");
                }
            }
            System.out.println();
            ch++;
        }
    }
}
