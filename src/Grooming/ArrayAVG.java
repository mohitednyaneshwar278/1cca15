package Grooming;

import javax.activation.MimeType;

public class ArrayAVG {
    public static void main(String[] args) {
        int [] arr={1,2,3,4,5,6,7,8,9,10};
        int sum=0;
        for(int a:arr){
            sum+=a;
        }
        double avg=sum/arr.length;

        System.out.println("AVG:  "+avg);
    }
}