package Grooming;

public class ArrayMaxNo {
    public static void main(String[] args) {
        int[] arr={10,20,30,40,50,60};
        int max=arr[0];
        for(int a:arr)
            if(max<a){
                max=a;
            }
        System.out.println("Max Array is: "+max);
    }
}
