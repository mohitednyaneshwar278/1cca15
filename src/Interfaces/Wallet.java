package Interfaces;
@FunctionalInterface
public interface Wallet {
    void makeBillPayment(double amt);
}
