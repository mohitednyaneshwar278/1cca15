package Interfaces;

import java.util.concurrent.Callable;

public class MainApp2 {
    public static void main(String[] args) {
        CreditCard credit;
        credit=new Visa();//upcasting
        credit.getType();
        credit.withdraw(2500);
        System.out.println("=================");
        credit=new MasterCard();//upcasting
        credit.getType();
        credit.withdraw(45);
    }
}
