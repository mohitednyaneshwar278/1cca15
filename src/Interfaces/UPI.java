package Interfaces;
@FunctionalInterface
public interface UPI {
    void tractionAmount(double amt);
}
