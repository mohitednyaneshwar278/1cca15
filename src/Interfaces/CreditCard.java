package Interfaces;

//super interface
public interface CreditCard {
    void getType();
    void withdraw(double amt);
}
