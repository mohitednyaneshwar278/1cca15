package Interfaces;

//implementation class
public class Visa implements CreditCard{

    @Override
    public void getType() {
        System.out.println("Credit Card Type is Visa");
    }

    @Override
    public void withdraw(double amt) {
        System.out.println("Traction successful of Rs"+amt);
    }
}
