package Interfaces;

//super interface
public interface BackEnd {
    void developServerProgram(String language);
}
