package Interfaces;

public class Software extends FrontEnd implements BackEnd,Database {
    @Override
    public void developServerProgram(String language) {
        System.out.println("Developing Server Program using"+language);
    }

    @Override
    public void databaseServer(String dbVendor) {
        System.out.println("Designing Database using "+dbVendor);
    }
}
