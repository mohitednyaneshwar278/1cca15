package Interfaces;

//implementation class
//Multiple inheritance
public class PhonePe implements UPI,Wallet {

    @Override
    public void tractionAmount(double amt) {
        System.out.println("Traction Amount "+amt);
    }

    @Override
    public void makeBillPayment(double amt) {
        System.out.println("Make Bill Payment of Rs"+amt);
    }
}
