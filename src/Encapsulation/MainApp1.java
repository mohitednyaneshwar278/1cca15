package Encapsulation;

public class MainApp1 {
    public static void main(String[] args) {
        Employee e1=new Employee();
        //read private data through getter
        int id=e1.getEmpId();
        double salary=e1.getEmpSalary();
        System.out.println("Id: "+id);
        System.out.println("Salary: "+salary);

        //modify private date through setter
        e1.setEmpId(201);
        e1.setEmpSalary(30000);
        System.out.println(e1.getEmpSalary());
        System.out.println(e1.getEmpId());
    }
}
