package Encapsulation;

//java bean class
public class Employee {
    private int empId=101;
    private double empSalary=12000;

    //Read access
    public int getEmpId() {
        return empId;
    }
     //write access
    public void setEmpId(int empId) {
        this.empId = empId;
    }
    //read access
    public double getEmpSalary() {
        return empSalary;
    }
    //write access
    public void setEmpSalary(double empSalary) {
        if(empSalary>0) {
            this.empSalary = empSalary;
        }else{
            System.out.println("Invalid Amount");
        }
    }
}
