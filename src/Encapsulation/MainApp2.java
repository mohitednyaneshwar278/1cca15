package Encapsulation;

public class MainApp2 {
    public static void main(String[] args) {
        Mobile m1=new Mobile();
        System.out.println("Company Name is "+m1.company);

        //Encapsulation
        Mobile.RAM r1=m1.new RAM();
        r1.displayInfo();

        //Encapsulation
        Mobile.Processor p1=m1.new Processor();
        p1.displayName();
    }
}
