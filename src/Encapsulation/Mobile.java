package Encapsulation;

//Outer class
public class Mobile {
    String company="SAMSUNG";

    //inner class
    class RAM{
        void displayInfo(){
            System.out.println("RAM Size is 8GB");
        }
    }
    //inner class
    class Processor{
        void displayName(){
            System.out.println("Processor Name is SNAPDRAGON");
        }
    }
}
