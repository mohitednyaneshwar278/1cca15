package Encapsulation;

public class MainApp3 {
    public static void main(String[] args) {
        //Run Time Implementation
        Shape s1=new Shape() {
            @Override
            public void area() {
                double result=3.14*5*5;
                System.out.println("Area of Circle is: "+result);
            }
        };
        s1.area();

        Shape s2=new Shape() {
            @Override
            public void area() {
                double result=0.5*4*5;
                System.out.println("Area of Triangle is: "+result);
            }
        };
        s2.area();
    }
}
