package Encapsulation;

@FunctionalInterface
public interface Shape {
    void area();
}
