package Encapsulation;
//Singleton class
public class GoogleAccount {
    //Singleton object
    static GoogleAccount g1;

    private GoogleAccount(){

    }

    //Singleton method
    static GoogleAccount login(){
        if(g1==null){
            g1=new GoogleAccount();
            System.out.println("LOGIN SUCCESSFUL");
            }
            else{
                System.out.println("ALREADY LOGGED IN");
            }
            return g1;
    }
    void accessGmail(){
        System.out.println("ACCESSING GMAIL");
    }

    void accessDrive(){
        System.out.println("ACCESSING DRIVE");
    }
}
