package StringClass;

public class StringDemo1 {
    public static void main(String[] args) {
        //constant pool area
        String s1="PUNE";
        String s2="MUMBAI";
        String s3="PUNE";

        //non-constant pool area
        String str1=new String("Mumbai");
        String str2=new String("PUNE");
        String str3=new String("PUNE");

        System.out.println(s1==s2);
        System.out.println(s1==s3);
        System.out.println(s1==str1);
        System.out.println(s1.equals(str2));
        System.out.println(str2==str3);
        System.out.println(s2.equals(str1));
        System.out.println(s2.equalsIgnoreCase(str1));
    }
}
