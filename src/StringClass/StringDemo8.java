package StringClass;

import java.util.Scanner;

public class StringDemo8 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Name");
        String name=sc.next();
        String username=name.trim();

        if(!username.isEmpty()){
            System.out.println("Enter Email");
            String email=sc.next();
            if(email.contains("@") && email.contains(".")){
                System.out.println("Enter Password");
                String pass=sc.next();
                if(pass.length()>=8 && !(pass.contains(username))){
                    System.out.println("Enter Conform password");
                    String conform=sc.next();
                    if(conform.equals(pass)){
                        System.out.println("Welcome  "+username.toUpperCase());
                        System.out.println("Register successful");
                    }
                    else {
                        System.out.println("Invalid Conform Password");
                    }
                }
                else{
                      System.out.println("Invalid password");
                    }
            }
            else{
                    System.out.println("Invalid Email");
                }
        }
        else{
                System.out.println("Invalid Name");
            }
    }
}
