package StringClass;

import java.util.Locale;
import java.util.Scanner;

public class StringDemo5 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Name");
        String name=sc.nextLine();
        String userName=name.trim();

        if(!userName.isEmpty()){
            System.out.println("Welcome "+name);
        }
        else {
            System.out.println("Invalid Name");
        }
    }
}
