package ExceptionHandling;

public class ExceptionDemo2 {
    public static void main(String[] args) {
        System.out.println("PROGRAM STARTED");
        String str=null;
        try{
            System.out.println(str.toLowerCase());
        }catch (Exception a){
            System.out.println(a);
        }
        System.out.println("PROGRAM ENDED");
    }
}
