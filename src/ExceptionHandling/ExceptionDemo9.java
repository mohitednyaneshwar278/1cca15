package ExceptionHandling;

import java.util.Scanner;

public class ExceptionDemo9 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Qty");
        int qty=sc.nextInt();
        System.out.println("Enter Price");
        double price=sc.nextDouble();
        billCalculation(qty,price);
    }
    static void billCalculation(int qty,double price){
        if(qty>0 && price>0){
            double total=qty*price;
            System.out.println("Total:"+total);
        }else {
            //explicit exception
            throw new IllegalArgumentException("Invalid Price or Qty");
        }
    }
}
