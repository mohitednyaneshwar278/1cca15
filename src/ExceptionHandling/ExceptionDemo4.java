package ExceptionHandling;

import ARRAYS.Array;

public class ExceptionDemo4 {
    public static void main(String[] args) {
        System.out.println("PROGRAM STARTED");
        int[] data=new int[3];
        String s=null;
        try {//Outer try Block
            try {//Inner try Block
                data[1] = 50;
                data[4] = 60;
                System.out.println(data[1] + "\t" + data[4]);
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println(e);
            }
            System.out.println(s.toLowerCase());
        }catch (NullPointerException a){
            System.out.println(a);
        }
        System.out.println("PROGRAM ENDED");
    }
}
