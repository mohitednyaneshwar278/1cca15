package ExceptionHandling;

import java.util.Scanner;

public class ExceptionDemo12 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Email Id");
        String email=sc.next();
        validation(email);
    }
    static void validation(String email){
        if(email.contains("@")&&email.contains(".")){
            System.out.println("Validation Successful");
        }else {
            throw new InvalidEmailException("Invalid Email Id");
        }
    }
}
