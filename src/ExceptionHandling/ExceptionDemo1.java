package ExceptionHandling;

public class ExceptionDemo1 {
    public static void main(String[] args) {
        System.out.println("PROGRAM STARTED");
        String s1="123ABC";
        try{
            int no=Integer.parseInt(s1);//abnormal
            System.out.println("NO IS "+no);
        }catch (NumberFormatException n){
            System.out.println(n);
        }
        System.out.println("PROGRAM ENDED");
    }
}
