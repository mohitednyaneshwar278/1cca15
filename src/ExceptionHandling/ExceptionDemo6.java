package ExceptionHandling;

public class ExceptionDemo6 {
    public static void main(String[] args) {
        System.out.println("PROGRAM STARTED");
        String str="JAVA";
        try{
            System.out.println("try Started");
            System.out.println(str.length());
            System.out.println("try Ended");
            //System.exit(0);
        }catch (NullPointerException e){
            System.out.println(e);
        }
        finally {
            System.out.println("Closing Costly Resources");
        }
        System.out.println("PROGRAM ENDED");
    }
}
