package ExceptionHandling;

import java.net.MalformedURLException;
import java.net.URL;

public class ExceptionDemo8 {
    public static void main(String[] args) {
        String link="https://www.google.com/search?qt20+world+cup+2022&rlz=1C1CHBF_enIN1005IN1008&oq=&aqs=chrome.0.35i39i362l8.92802705j0j7&sourceid=chrome&ie=UTF-8";
        try{
            URL url=new URL(link);
            System.out.println(url.getHost());
            System.out.println(url.getProtocol());
            System.out.println(url.getPath());
        }catch(MalformedURLException e){
            System.out.println(e);
        }
    }
}
