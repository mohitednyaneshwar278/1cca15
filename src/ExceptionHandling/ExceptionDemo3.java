package ExceptionHandling;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionDemo3 {
    public static void main(String[] args) {
        System.out.println("PROGRAM STARTED");
        Scanner sc = new Scanner(System.in);
        try {
            System.out.println("Enter Number 1");
            int no1 = sc.nextInt();
            System.out.println("Enter Number 2");
            int no2 = sc.nextInt();
            int result = no1 / no2;
            System.out.println("Result: " + result);
        } catch (ArithmeticException a) { //subclass
            System.out.println(a);
        } catch (InputMismatchException i) {//super class
            System.out.println(i);
        }
        System.out.println("PROGRAM ENDED");
    }
}