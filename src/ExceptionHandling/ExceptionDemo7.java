package ExceptionHandling;

import java.io.FileWriter;
import java.io.IOException;

//checked exception Scenario
public class ExceptionDemo7 {
    public static void main(String[] args) {
        String msg="JAVA IS OBJECT ORIENTED PROGRAMMING LANGUAGE";
        FileWriter fw=null;
        try{
            fw=new FileWriter("D://FileData//info.txt");
            fw.write(msg);
            fw.close();
            System.out.println("File Created Successfully");
        }catch(IOException e){
            System.out.println(e);
        }
    }
}
