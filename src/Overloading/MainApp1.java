package Overloading;

import java.util.Scanner;

public class MainApp1 {
    public static void main(String[] args) {
        Student s1=new Student();
        Scanner sc=new Scanner(System.in);
        System.out.println("Select Search criteria");
        System.out.println("1:Search Student Name");
        System.out.println("2:Search Student Contact");
        int choice=sc.nextInt();

        if(choice==1){
            System.out.println("Enter Name");
            String name=sc.next();
            s1.search(name);
        }
        else if(choice==2){
            System.out.println("Enter Contact");
            int contact=sc.nextInt();
            s1.search(contact);
        }
        else{
            System.out.println("INVALID CHOICE");
        }

    }
}
