package Overloading;

import jdk.nashorn.internal.runtime.regexp.joni.ast.StringNode;

public class Facebook {
    String email="abc@gmail.com";
    double contact=12345;
    String password="4545";

    void login(String emailId,String pass){
        if(emailId.equalsIgnoreCase(email)&&pass.equalsIgnoreCase(password)){
            System.out.println("EMAIL ID IS:"+email);
            System.out.println("PASSWORD IS:"+password);
        }
        else {
            System.out.println("PASSWORD IS INCORRECT");
        }
    }
    void login(double cont, String pass){
        if(cont==contact&&pass.equalsIgnoreCase(password)){
            System.out.println("CONTACT IS:"+contact);
            System.out.println("PASSWORD IS:"+password);
        }
        else{
            System.out.println("PASSWORD IS INCORRECT");
        }
    }
}
