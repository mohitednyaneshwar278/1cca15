package Overloading;

public class MainApp4 {
    public static void main(String[] args) {
    // STANDARD MAIN METHOD
        System.out.println("MAIN METHOD.1");
        main(25);
        main('j');
    }

    //EXTERNAL METHOD OR NON-STANDARD MAIN METHOD
    public static void main(int a) {
        System.out.println("A:"+a);
        System.out.println("MAIN METHOD.2");
    }

    public static void main(char c) {
        System.out.println("C:"+c);
        System.out.println("MAIN METHOD.3");
    }
}
