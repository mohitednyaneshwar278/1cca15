package Abstraction;


import java.util.Scanner;

public class MainApp2 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Select Account type");
        System.out.println("1:Saving\n2:Loan");
        int accType=sc.nextInt();
        System.out.println("Enter Account Opening Balance");
        double balance=sc.nextDouble();

        AccountFactory factory=new AccountFactory();
        Account accRef= factory.createAccount(accType,balance);

        boolean status=true;
        while(status){
            System.out.println("Select Mode Of Transaction");
            System.out.println("1:Deposit\n2:withdraw\n3CheckBalance\n4:Exit");
            int choice=sc.nextInt();
            if(choice==1){
                System.out.println("Enter Amount");
                double amt= sc.nextDouble();
                accRef.deposit(amt);
            } else if (choice==2) {
                System.out.println("Enter Amount");
                double amt= sc.nextDouble();
                accRef.withdraw(amt);
            } else if (choice==3) {
                accRef.checkBalance();
            }else{
                status=false;
            }
        }
    }
}
