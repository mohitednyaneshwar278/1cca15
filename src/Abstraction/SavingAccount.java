package Abstraction;

//Step-2
public class SavingAccount implements Account{
    double accountBalance;
    //Account Creation

    public SavingAccount(double accountBalance) {
        this.accountBalance = accountBalance;
        System.out.println("Saving Account Created");
    }

    @Override
    public void deposit(double amt) {
        accountBalance+=amt;
        System.out.println(amt+"Rs Credited To Your Account");
    }

    @Override
    public void withdraw(double amt) {
        if(amt<=accountBalance){
            accountBalance-=amt;
            System.out.println(amt+"Rs Debited From Your Account");
        }
    }

    @Override
    public void checkBalance() {
        System.out.println("Active Balance"+accountBalance);
    }
}
