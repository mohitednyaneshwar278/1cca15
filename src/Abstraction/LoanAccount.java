package Abstraction;

//Step-3
public class LoanAccount implements Account{
    double LoanAmount;
    //account creation

    public LoanAccount(double loanAmount) {
        this.LoanAmount = loanAmount;
        System.out.println("Loan Account Created");
    }

    @Override
    public void deposit(double amt) {
        LoanAmount-=amt;
        System.out.println(amt+"Rs Debited From Loan Account");
    }

    @Override
    public void withdraw(double amt) {
        LoanAmount+=amt;
        System.out.println(amt+"Rs Created  to your Account");
    }

    @Override
    public void checkBalance() {
        System.out.println("Active Loan amount"+LoanAmount);
    }
}
