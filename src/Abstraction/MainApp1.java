package Abstraction;

//Utilization Layer
public class MainApp1 {
    public static void main(String[] args) {
        Switch s;
        s=new LedLight();//upcasting
        s.SwitchOn();
        s.SwitchOff();

        s=new CflLight();//upcasting
        s.SwitchOn();
        s.SwitchOff();
    }
}
