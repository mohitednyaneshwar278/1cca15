package Abstraction;

//Step-1 create an interface
public interface Account {
    void deposit(double amt);
    void withdraw(double amt);
    void checkBalance();
}
