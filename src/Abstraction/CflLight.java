package Abstraction;

//implementation class
public class CflLight implements Switch{
    @Override
    public void SwitchOn() {
        System.out.println("CEF LIGHT SWITCH ON");
    }

    @Override
    public void SwitchOff() {
        System.out.println("CEF LIGHT SWITCH OFF");
    }
}
