package Inheritance;

public class MainApp2 {
    public static void main(String[] args) {
        //object of latest of subclass
        WhatsAppV3 v=new WhatsAppV3();
        v.chatting();
        v.calling();
        v.payment();
    }
}
