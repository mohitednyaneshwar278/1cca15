package Inheritance;

//subclass
public class Department extends College{
    Department(String universityName,String collegeName,String departmentName){
        super(universityName,collegeName);
        System.out.println("DEPARTMENT:"+departmentName);
    }
}
