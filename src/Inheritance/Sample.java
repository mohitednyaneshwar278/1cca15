package Inheritance;

//subclass
public class Sample extends Demo {

    public Sample(){
        //super() --->Declared by compiler
        System.out.println("SUB CLASS CONSTRUCTOR");
    }
}
