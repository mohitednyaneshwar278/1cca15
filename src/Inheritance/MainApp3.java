package Inheritance;

public class MainApp3 {
    public static void main(String[] args) {

        PermenantEmployee p1=new PermenantEmployee();
        p1.getInfo(101,30000);
        p1.getDesignation("Analyst");

        System.out.println("====================");

        ContractEmployee c1=new ContractEmployee();
        c1.getInfo(201,40000);
        c1.getContractDetails(24);
    }
}
