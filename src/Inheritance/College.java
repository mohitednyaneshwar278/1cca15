package Inheritance;

//subclass
public class College extends University{
    College(String universityName,String collegeName){
        super(universityName);
        System.out.println("COLLEGE:"+collegeName);
    }
}
