package Inheritance;

public class Mobile extends Product{
    double productPrice = 35000;
    int qty = 10;

    void info() {
        System.out.println("PRICE:" + productPrice);
        System.out.println("QTY:" + qty);
        System.out.println("PRICE:"+super.productPrice);
        System.out.println("QTY:"+super.qty);
    }
}
