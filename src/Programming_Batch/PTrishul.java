package Programming_Batch;

public class PTrishul {
    public static void main(String[] args) {
        int line=7;
        int star=line/2+1;

        for(int i=1;i<=line;i++){
            for(int j=1;j<=line;j++){
                if(i==star||j==star||  (j==1&&i<=star)||
                        (j==7&&i<=4)){
                    System.out.print("* ");
                }else{
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
}
