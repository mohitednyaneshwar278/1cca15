package Programming_Batch;

public class MaxNo {
    public static void main(String[] args) {
        int[] arr={1,3,4,10,8,9};
        int max=arr[0];
        for(int a:arr){
            if(a>max){
                max=a;
            }
        }
        System.out.println("Max:"+max);
    }
}
