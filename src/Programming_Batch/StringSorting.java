package Programming_Batch;

public class StringSorting {
    public static void main(String[] args) {
        int[] arr={8,5,2,6,1};
        for(int a:arr)
            System.out.print(a + " ");
        System.out.println();
        System.out.println("==========");

        for(int i=0;i<arr.length;i++)
        {
            int minInx=i;
            for(int j=i+1;j<arr.length;j++)
            {
                if(arr[j]<arr[minInx])
                {
                    minInx=j;
                }
                int temp=arr[i];
                arr[i]=arr[minInx];
                arr[minInx]=temp;
            }
            System.out.println();
            for(int a:arr){
                System.out.print(a+" ");
            }
        }
    }
}
