package Programming_Batch;

public class Program21 {
    public static void main(String[] args) {
        int line = 3;
        int star = 4;
        int ch = 1;
        for (int i = 0; i < line; i++) {
            for(int j=0;j<star;j++){
                System.out.print(ch+" ");
            }
            System.out.println();
            for(int k=0;k<4;k++) {
                System.out.print("*"+" ");
            }
            System.out.println();
            ch++;
        }
    }
}