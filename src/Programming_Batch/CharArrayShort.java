package Programming_Batch;

public class CharArrayShort {
    public static void main(String[] args) {
        char[] arr={'z','c','m','t','y'};
        for(char i=0;i<arr.length;i++) {

            for (char j=0;j< arr.length-1;j++) {
                if (arr[j] > arr[j + 1]) {
                    char temp=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=temp;
                }
            }
        }
        for(char a:arr){
            System.out.print(a+" ");
        }
    }
}
