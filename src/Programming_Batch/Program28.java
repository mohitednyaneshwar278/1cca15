package Programming_Batch;

public class Program28 {
    public static void main(String[] args) {
        int star=1;
        int line=9;
        int space=4;
        for(int i=0;i<line;i++){
            char ch='A';
            for(int j=0;j<space;j++){
                System.out.print("   ");
            }
            for(int k=0;k<star;k++){
                System.out.print(" "+ch+" ");
                ch++;
            }

            System.out.println();
            if(i<=3) {
                space--;
                star++;
            }
            else{
                star--;
                space++;
            }
        }

    }
}
