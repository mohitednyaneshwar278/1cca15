package Programming_Batch;

public class Program27 {
    public static void main(String[] args) {
        int star=1;
        int line=5;
        int space=line-1;
        int ch1=5;
        for(int i=0;i<line;i++){
            int ch2=ch1;
            for(int j=0;j<space;j++){
                System.out.print("   ");
            }
            for(int k=0;k<star;k++){
                System.out.print(" "+ch2+" ");
                ch2++;
            }
            System.out.println();
            ch1--;
            space--;
            star++;
        }

    }
}
