package Programming_Batch;

public class ArmStrongNo {
    public static void main(String[] args) {
        int a=153;
        int sum=0;
        while(a!=0) {
            int r = a % 10;
            sum += (r * r * r);
            a = a / 10;
        }
        if(sum==153){
            System.out.println("ArmStrong Number");
        }else{
            System.out.println("Not ArmStrong Number");
        }
    }
}
