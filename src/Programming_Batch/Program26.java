package Programming_Batch;

public class Program26 {
    public static void main(String[] args) {
        int star=1;
        int line=5;
        int space=line-1;
        for(int i=0;i<line;i++){
            int ch=1;
            for(int j=0;j<space;j++){
                System.out.print("   ");
            }
            for(int k=0;k<star;k++){
                System.out.print(" "+ch+" ");
                ch++;
            }
            System.out.println();
            space--;
            star++;
        }

    }
}
