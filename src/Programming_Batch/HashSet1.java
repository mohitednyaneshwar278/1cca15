package Programming_Batch;

import java.util.HashSet;
import java.util.Set;

public class HashSet1 {
    public static void main(String[] args) {
        Set<Integer> hs1=new HashSet<>();


        hs1.add(10);
        hs1.add(20);
        hs1.add(30);
        for(Integer i:hs1){
            System.out.println(i);
        }
        Set<Character> hs2=new HashSet<>();
        String str="CORE JAVA PROGRAMMING";
        for(int i=0;i<str.length();i++){
            hs2.add(str.charAt(i));
        }
        for(Character c:hs2){
            System.out.println(c);
        }
    }
}
