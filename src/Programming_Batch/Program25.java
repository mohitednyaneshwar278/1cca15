package Programming_Batch;

public class Program25 {
    public static void main(String[] args) {
        int star=1;
        int line=5;
        int space=line-1;
        for(int i=0;i<line;i++){
            for(int j=0;j<space;j++){
                System.out.print(" _ ");
            }
            for(int k=0;k<star;k++){
                System.out.print(" * ");
            }
            System.out.println();
            space--;
            star++;
        }

    }
}
