package Programming_Batch;

import java.util.Scanner;

//convert row into column
public class MatrixArray {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);

        System.out.println("Enter Row value");
        int row=sc.nextInt();
        System.out.println("Enter columns value");
        int col=sc.nextInt();
        int[][]arr=new int[row][col];
        int temp[][]=new int [row][col];
        System.out.println("Enter array values");
        for(int i=0;i<row;i++){
            for(int j=0;j<col;j++){
                arr[i][j]=sc.nextInt();
            }
        }
        for(int i=0;i<row;i++){
            for(int j=0;j<col;j++){
                System.out.print(arr[i][j]+"\t");
            }
            System.out.println();
        }
        System.out.println();
        //reverse array
        for(int i=0;i<row;i++){
            for(int j=0;j<col;j++){
                temp[j][i]=arr[i][j];
            }
        }
        for(int i=0;i<row;i++) {
            for (int j = 0; j < col; j++) {
                System.out.print(temp[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
