package Programming_Batch;

public class Program23 {
    public static void main(String[] args) {
        int line=5;
        int star=6;

        for(int i=0;i<line;i++){
            int ch=1;
            for(int j=0;j<star;j++) {
                if (!(j== 1 || j == 3||j==5)) {
                    System.out.print(ch+" ");
                } else {
                    System.out.print("*"+" ");
                }
                ch++;
                }
            System.out.println();
        }
    }
}
