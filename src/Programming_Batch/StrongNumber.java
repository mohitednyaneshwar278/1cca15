package Programming_Batch;

public class StrongNumber {
    public static void main(String[] args) {
       int a=145;
       int temp=a;
       int sum=0;
       while(a!=0){
           int r=a%10;
           int fact=1;
           for(int i=1;i<=r;i++) {
               fact *= i;
           }
               sum+=fact;
               a/=10;
           }
           if(sum==temp){
               System.out.println("The Strong Number is:  "+sum);

       }
    }
}
