package Programming_Batch;

public class Reverse {
    public static void main(String[] args) {
        int a=1234;
        int sum=0;
        while(a!=0){
            int r=a%10;
            sum=(sum*10)+r;
            System.out.print(r);
            a/=10;
        }
        System.out.println("   Sum: "+sum);
    }
}
