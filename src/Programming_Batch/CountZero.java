package Programming_Batch;

public class CountZero {
    public static void main(String[] args) {
        long a=111000010250500020l;
        int count=0;
        while(a!=0)
        {
            int r= (int) (a%10);
            if(r==0)
                count++;
            a=a/10;
        }
        System.out.println(count);
    }
}
