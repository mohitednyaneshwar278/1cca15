package Programming_Batch;

public class SecondMax {
    public static void main(String[] args) {
        int[] arr = {2, 11, 6, 8, 10};
        int max1 = arr[0];
        int max2 = arr[1];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max1) {
                max2 = max1;
                max1 = arr[i];
            } else if (arr[i] > max2) {
                max2 = arr[i];
            }
        }
        System.out.println("max1:" + max1);
        System.out.println("Max2:" + max2);
    }
}
