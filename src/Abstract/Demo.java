package Abstract;

public abstract class Demo {
    //static & non-static variable
    static int k=20;
    double d=35.25;

    //non-static abstract & concrete method
    abstract void test();
    void display(){
        System.out.println("Display method");
    }

    //static concrete methods
   static void info() {
        System.out.println("Info Method");
    }

    //constructor
    Demo(){
        System.out.println("Constructor");
    }

    //static & non-static blocks
    static {
        System.out.println("Static Block");
    }

    {
        System.out.println("non-static Block");
    }
}
