package Abstract;

//super class
public abstract class InstagramV1 {
    void makePost(){
        System.out.println("Post Photo or Video");
    }
    abstract void stories();
    abstract void reels();
}
