package Abstract;

public class Manager extends Employee{

    @Override
    void getDesignation() {
        System.out.println("Designation is Manager");
    }

    @Override
    void getSalary() {
        System.out.println("Salary is 1500000");
    }
}
