package Abstract;

public abstract class Employee {
    abstract void getDesignation();
    abstract void getSalary();
}
