package Casting;

public class MainApp7 {
    public static void main(String[] args) {
        Bike b = new ElectricBike();//upcasting
        b.getType();
        System.out.println("===============");
        ElectricBike e = (ElectricBike) b;//down casting
        e.getType();
        e.batteryInfo();
    }
}
