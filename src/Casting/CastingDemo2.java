package Casting;

public class CastingDemo2 {
    public static void main(String[] args) {
        int x=53;
        float y=35.4f;

        int a=(int)y;//narrowing
        float b=x;//widening
        System.out.println(a+"\t\t"+b);
    }
}
