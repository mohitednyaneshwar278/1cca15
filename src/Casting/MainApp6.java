package Casting;

public class MainApp6 {
    public static void main(String[] args) {
        Master m1=new Master();//object of superclass
        if(m1 instanceof Central){
          Central c1=(Central) m1;
        }else{
            System.out.println("Properties not Found");
        }
    }
}
