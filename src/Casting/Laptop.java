package Casting;

public class Laptop extends Machine{
    @Override
    void getType() {
        System.out.println("Machine Type is Laptop");
    }

    @Override
    void calculateBill(int qty, double price) {
        //15% gst
       double total=qty*price;
       double finalAmt=total+total*0.15;
       System.out.println("Final Amt: "+finalAmt);
    }
}
