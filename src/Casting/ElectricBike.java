package Casting;

//subclass
public class ElectricBike extends Bike{
    @Override
    void getType() {
        System.out.println("Bike Type is Electric");
    }

    void batteryInfo(){
        System.out.println("Battery Size is 100 Watt");
    }
}
