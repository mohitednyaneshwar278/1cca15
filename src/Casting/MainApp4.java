package Casting;

import java.util.Scanner;

public class MainApp4 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Select Service Provider");
        System.out.println("1:AirAsia\n2:Indigo");
        int choice=sc.nextInt();
        System.out.println("Select Route");
        System.out.println("0:Pune to Mumbai");
        System.out.println("1:Mumbai to Chennai");
        System.out.println("2:Kolkata to Bangalore");
        int routeChoice=sc.nextInt();
        System.out.println("Enter no of Tickets");
        int tickets=sc.nextInt();

        Goibibo g1=null;
        if(choice==1){
            g1=new AirAsia();//upcasting
        }else if(choice==2){
            g1=new Indigo();//upcasting
        }
        g1.bookTicket(tickets,routeChoice);
    }
}
