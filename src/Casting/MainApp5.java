package Casting;

public class MainApp5 {
    public static void main(String[] args) {
        Master m1=new Master();
        m1.test();
        Central c1=(Central) new Master();//downcasting
        c1.test();
        c1.display();
    }
}
