package Casting;

import java.util.Scanner;

public class MainApp3 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Qty");
        int qty=sc.nextInt();
        System.out.println("Enter Price");
        double price=sc.nextDouble();
        System.out.println("Select Machine");
        System.out.println("1:Laptop\n2:Projector");
        int choice=sc.nextInt();

        Machine m1=null;
        if(choice==1){
            m1=new Laptop();//upcasting
        }else if(choice==2){
            m1=new Projector();//upcasting
        }
        m1.getType();
        m1.calculateBill(qty,price);
    }
}
