package Casting;

public class Projector extends Machine{
    @Override
    void getType() {
        System.out.println("Machine Type is Projector");
    }

    @Override
    void calculateBill(int qty, double price) {
        //10% gst
        double total=qty*price;
        double finalAmt=total+total*0.1;
        System.out.println("Final Amt: "+finalAmt);
    }
}
