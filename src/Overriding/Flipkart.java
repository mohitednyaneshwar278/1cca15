package Overriding;

//subclass
public class Flipkart extends Ecommerce{
    @Override
    void sellProduct(double price, int qty) {
        //10% Discount
        double total=qty*price;
        double finalAmt=total-total*0.1;
        System.out.println("Final Amt:"+finalAmt);
    }
}
