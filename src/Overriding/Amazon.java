package Overriding;

//Subclass
public class Amazon extends Ecommerce{
    @Override
    void sellProduct(double price, int qty) {
        //5% Discount
        double total=qty*price;
        double finalAmt=total-total*0.05;
        System.out.println("Final Amt:"+finalAmt);
    }
}
