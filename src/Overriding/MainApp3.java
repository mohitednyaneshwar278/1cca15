package Overriding;

import java.util.Scanner;

public class MainApp3 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter price");
        double price=sc.nextDouble();
        System.out.println("Select Mobile Application");
        System.out.println("1:PhonePe\n2:GooglePe");
        int choice=sc.nextInt();

        if(choice==1){
            PhonePe p1=new PhonePe();
            p1.recharge(price);
        }
        else if (choice==2) {
            GooglePe g1=new GooglePe();
            g1.recharge(price);
        }
        else{
            System.out.println("Invalid choice");
        }
    }
}
